/*
*      Program to calculate the checksum of a file
*      Stolen wholesale from RFC 1950
*/
 
/*
* We need this to read large files on a 32 bit system.
* So we need to defile _FILE_OFFSET_BITS
* Allegedly it does not do any harm on 64 bit systems
*/
#define _FILE_OFFSET_BITS 64
 
/* Standard includes */
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
 
#define BUFFER_SIZE (1024 * 1024) /* Use 1M buffers */
#define MOD_ADLER 65521
 
 
unsigned long int update_adler32 (unsigned long adler, unsigned char *data, size_t len)
{
    unsigned long int  a = adler & 0xffff;
    unsigned long int  b = (adler >> 16) & 0xffff;
    int  index ;
 
    /* Loop over each byte of data, in order */
    for (index = 0; index < len; ++index)
    {
        a = (a + data[index]) % MOD_ADLER;
        b = (b + a) % MOD_ADLER;
    }
 
    return (b << 16) + a ;
}
 
 
int main(int argc, char *argv[])
{
        char buffer[BUFFER_SIZE];                       /* buffer for data */
        char *filename =  argv[1];      /* name of our input file */
        int in_file;                    /* input file descriptor */
        int read_size;                  /* number of bytes on last read */
        int file_size = 0 ;             /* file size  */
        unsigned long int  adler = 1L;  /* number of adles */
 
        if (argc != 2) {
                fprintf(stderr, "Error: Wrong number of arguments.\n");
                fprintf(stderr, "Error: usage is ... \n");
                fprintf(stderr, "\t%s  <filename>\n", argv[0]);
                exit (8);
        }
 
        in_file = open(argv[1], O_RDONLY, "r");
        if (in_file < 0 ) {
                fprintf(stderr, "Error: unable to open %s.\n", argv[1]);
                exit (8);
        }
 
        while ( read_size = read(in_file, buffer, sizeof(buffer)))
                {
 
                if (read_size == 0 )
                        break ;         /* EOF */
 
                if (read_size <  0 ) {
                        fprintf(stderr, "Error: Read error.\n");
                        exit (8) ;
                }
 
                file_size += read_size ;
 
                adler = update_adler32(adler, buffer, read_size);
        }
        /*      printf ("%s\t%d\t%08lx\n", filename, file_size, adler); */
        /*      We only want the hex checksum here                      */
        /*      So comment out the above and just print the checksum    */
                printf ("%08lx\n", adler);
 
 
 
        return (0) ;
}
