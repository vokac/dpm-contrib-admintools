# dpm-tools python module
# Provides some basic shared functionality used by the gridpp-dpm-tool admin tool set
#
# Samuel C Skipsey 2011
# This file is licensed for use under the BSD licence.

import sys
import os
try:
        import MySQLdb
except:
        sys.exit("Could not import MySQLdb module. Please install the MySQL Python module.")


def MySQLConnect(dpm_db=False):
	#if dpm_db:
	#	configfile = "DPMCONFIG"
	#	defaultdb = "dpm_db"
	#else:
	configfile = "NSCONFIG"
	defaultdb = "cns_db"

	#need to also check NSCONFIG, which apparently exists and is used by people
        try:
                dpminfopath = os.environ['LCG_LOCATION'].rstrip('/') + '/etc/' + configfile
        except:
                dpminfopath = '/opt/lcg/etc/' + configfile #CONFIG, not INFO, because the CONFIG user has dpm_db rights
        try:
                dpminfo = file(dpminfopath)
        except:
		dpminfopath = '/usr/etc/' + configfile
		try:
			dpminfo = file(dpminfopath)
		except:
	                sys.exit('No DPMINFO or NSCONFIG file found: cannot guess password for database ;)')
	                #sys.exit(1)
        (up,hd) = dpminfo.readline().split('@',1)
        (u,p) = up.split('/',1)

        try:
                (h,d) = hd.split('/',1)
                d=d.rstrip()
        except:
                h=hd.rstrip()
                d=defaultdb
        try:
                c = MySQLdb.connect(host=h, user=u, passwd=p, db=d.strip())
                cc = c.cursor()
        except MySQLdb.Error, e:
                sys.exit("Error %d: %s" % (e.args[0], e.args[1]))
                #sys.exit (1)

	if dpm_db: #parse the DPM DB config file (usually "DPMCONFIG") to get the *name* of the dpm_db if it has a different name
		try:	
			dpmconfigpath = os.environ['LCG_LOCATION'].rstrip('/') + '/etc/' + "DPMCONFIG"
        	except:
                	dpmconfigpath = '/opt/lcg/etc/' + "DPMCONFIG" #CONFIG, not INFO, because the CONFIG user has dpm_db rights		
		try:
			dpmconfig = file(dpmconfigpath)
		except:
			dpmconfigpath = '/usr/etc/DPMCONFIG'
			try:
				dpmconfig = file(dpmconfigpath)
			except:	
				sys.exit("No DPMCONFIG file found: can't guess name of dpm database")
		(_,hd) = dpmconfig.readline().split('@',1)
		try:
			(_,d) = hd.split('/',1)
			d=d.rstrip()
		except:
			d="dpm_db"
		return c,cc,d

        return c,cc

def vouser_lookupquery(cc,vo='%',uname='%'):
	gidstr = ''
	uidstr = ''
	wh = ''
	conj = ''
	uiddict = None
	giddict = None
	gids = None
	uids = None
	if (vo is not None):
		cc.execute('''SELECT Cns_groupinfo.gid, Cns_groupinfo.groupname from Cns_groupinfo WHERE Cns_groupinfo.groupname LIKE '%s' ''' % (vo));
		tmp = cc.fetchall()
		
		# the results are returned as a tuple of tuples (with only one element per tuple since we only asked for one column)
		gids = [(i[0],i[1]) for i in tmp]
		giddict = dict(gids)
		gidstr = 'cns_db.Cns_file_metadata.gid IN ('+','.join([str(i[0]) for i in gids])+')'
	if (uname is not None):
		cc.execute('''SELECT Cns_userinfo.userid, Cns_userinfo.username from Cns_userinfo WHERE Cns_userinfo.username LIKE '%s' ''' % (uname));
		tmp = cc.fetchall()
		uids = [(i[0],i[1]) for i in tmp]
                uiddict = dict(uids)
		uidstr = 'Cns_file_metadata.owner_uid IN ('+','.join([str(i[0]) for i in uids])+')'
	if (uidstr != '' or gidstr != ''):
		wh = ' WHERE '
	if (uidstr != '' and gidstr != ''):
		conj = ' AND '
	querypart = '''%s%s%s%s''' % (wh,uidstr,conj,gidstr)
	
	#now build a dictionary of uid:username, gid:groupname mappings to return as well	
	#print uids 
	#uiddict = dict(uids)
	#giddict = dict(gids)
	return (uiddict,giddict,querypart)


def fileid_lookup(cc,fileid):
	namelist = ['']
        try:
                cc.execute('''
 select parent_fileid, name from Cns_file_metadata where Cns_file_metadata.fileid = "%s"''' % fileid)

                name = ''
                parent_fileid = 0L
                p = cc.fetchone()
                #handle zero results here by raising exception!!!
                (parent_fileid, name) = p #this gets the "head" of the namei
                namelist.append(str(name))
                while parent_fileid > 1:
                        cc.execute('''select parent_fileid, name from Cns_file_metadata where Cns_file_metadata.fileid = %s''' % parent_fileid)
                        (parent_fileid, name) = cc.fetchone()
                        #the above fetchone() is a zero length tuple in the case of no results, so that should except?
                        namelist.append(str(name))
        except MySQLdb.Error, e:
                sys.exit("Error %d: %s" % (e.args[0], e.args[1]))
                #sys.exit (1)
        namelist.reverse() #put entries in "right" order for joining together
        return '/'.join(namelist)[1:-1] #and print dpns name (minus srm bits)



def sfn_lookup(cc, sfn):
	try:
		cc.execute('''select fileid from Cns_file_replica WHERE Cns_file_replica.sfn="%s"''' % sfn)
		fileid = cc.fetchone()[0] #test this
		return fileid_lookup(cc,fileid)
	except MySQLdb.Error, e:
		sys.exit("Error %d: %s" % (e.args[0],e.args[1]))


def parse_size(size,si):
        '''Utility function to correctly generate file size prefixes'''
        s = int(size)
        if si:
                divisor = 1000.0
        else:
                divisor = 1024.0
        index = 0
        prefixes = ('B','K','M','G','T','P','E','Z','Y')
        while (s >= divisor):
                index+=1
                s = s/divisor

        return str(s) + prefixes[index]

#        namelist = ['']
#        try:
#                cc.execute('''
# select parent_fileid, name from Cns_file_replica JOIN Cns_file_metadata ON Cns_file_replica.fileid = Cns_file_metadata.fileid WHERE Cns_file_replica.sfn="%s"''' % sfn)
#
#                name = ''
#                parent_fileid = 0L
#                p = cc.fetchone()
#                #handle zero results here by raising exception!!!
#                (parent_fileid, name) = p #this gets the "head" of the namei
#                namelist.append(str(name))
#                while parent_fileid > 1:
#                        cc.execute('''select parent_fileid, name from Cns_file_metadata where Cns_file_metadata.fileid = %s''' % parent_fileid)
#                        (parent_fileid, name) = cc.fetchone()
#                        #the above fetchone() is a zero length tuple in the case of no results, so that should except?
#                        namelist.append(str(name))
#        except MySQLdb.Error, e:
#                sys.exit("Error %d: %s" % (e.args[0], e.args[1]))
#                #sys.exit (1)
#        namelist.reverse() #put entries in "right" order for joining together
#        return '/'.join(namelist)[1:-1] #and print dpns name (minus srm bits)
